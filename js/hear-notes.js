function dhnl_init() {
    var iframes = elementList('iframe');
    for (var i = 0; i < iframes.length; i++) {
        iframes[i].className = "hide";
    }

}

function toggleShowHide(id) {
    var element = document.getElementById(id);
    if (element.className == 'hide') {
        element.className = 'show';
    } else {
        element.className = 'hide';
    }
}

function hideElement(id) {
    document.getElementById(id).style = "display: none;";
}

function showElement(id) {
    document.getElementById(id).style = "display: block;";
}

function setIframe(id, aid, exnr) {
    dhnl_init();
    var element = document.getElementById(id);
    var head = document.getElementById('h1');
    head.innerHTML = 'Øvelse ' + exnr;
    var url = 'https://notes.matchmysound.com/embed.html#?ass_id=' + aid;
    element.src = url;
    toggleShowHide(id);
}

function elementList(type) {
    var list = document.getElementsByTagName(type);
    //for ( var i = 0; i < list.length; i++ ) {
    //    alert(list[i].id);
    //}
    return document.getElementsByTagName(type);
}
// (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/5
function mmsProblems() {
    var regex = /ipad|iphone/i;
    var appVer = navigator.platform;
    if ( regex.test(appVer) || /Edge/i.test(navigator.appVersion) || 
       (/Macintosh/.test(navigator.appVersion) && /Safari/.test(navigator.appVersion) && !/Chrome/.test(navigator.appVersion) ))     {
        getWP_Page('mms-virker-ikke');
    }

}

function getWP_Page(slug) {
    var mmsPageObj;
    var url = document.URL;
    url = siteURL + '/wp-json/wp/v2/pages/?slug=' + slug;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onload = function () {
        if (this.readyState == 4 && this.status == 200) {
            mmsPageObj = JSON.parse(this.responseText);
            var mmsText = mmsPageObj[0].content.rendered;
            document.getElementById("mmsProblem").innerHTML = mmsText;
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}
