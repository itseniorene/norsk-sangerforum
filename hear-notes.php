<?php
error_reporting(E_ALL);
/*
Plugin Name: Digital Hearing and Note Learning
Plugin URI: https://itseniorene.no
Description: Obtain corresponding exercises from Match My Sound Collection
Version: 0.1
Author: IT Seniorene AS
Author URI: https://itseniorene.no
Text Domain: dhnl
 */

if (!function_exists('wp_enqueue_scripts')) die('xxxxxxxxxxxx');

class DigitalHearing
{
    private $mydir;
    private $siteURL;
    // private $static_data;

    public function __construct()
    {
        //spl_autoload_register([$this, 'autoload_classes']);
        $this->mydir = dirname(__FILE__);
        add_shortcode('hear-notes', [$this, 'app_control']);
        add_action('wp_enqueue_scripts', [$this, 'add_scripts']);
    }

    function add_scripts()
    {
        global $post;
        $ver = '1.0.0';
        $match;
        if (!preg_match('/\[hear-notes.*\]/', $post->post_content, $match)) {
            return false;
        }
        $url = plugins_url('css/hear-notes.css', __FILE__);
        wp_enqueue_style('hear-notes-style', $url, [], $ver);
        $url = plugins_url('js/hear-notes.js', __FILE__);
        wp_enqueue_script('js/hear-notes', $url, [], $ver);
        wp_enqueue_script("jquery");
    }

    public function app_control($atts)
    {
        global $wp;
        global $wpdb;

        $this->siteURL = site_url();
        $efs_atts = shortcode_atts([
            'action' => 'get',
            'title' => '{empty}'
        ], $atts);
        $action = $efs_atts['action'];
        $title = $efs_atts['title'];
        ob_start();
        $row = $this->getAssignmentId($title);
        if ($row == null) {
            $emsg = wp_sprintf("Øvelsen med assigment-id %s ble ikke funnet!", $title);
            $result = $this->showErrorPage($emsg);
        } else {
            if (count($row) == 1) {
                $ass_id = $row[0]['dhnl_aid'];
                $result = $this->showIframe($ass_id);
            } else {
                $result = $this->showExercises($row);
            }
        }
        return $result;
    }

    private function showIframe($ass_id)
    {
        $partOne = file_get_contents($this->mydir . "/html/mmsStartPagejs.html");
        $partOne .=  "<script>var siteURL = '$this->siteURL'</script>" . PHP_EOL;
        $partTwo = file_get_contents($this->mydir . "/html/mmsIframe.html");
        $partTwo = wp_sprintf($partTwo, $ass_id);
        return $partOne . $partTwo;
    }

    private function showExercises($rows) {
        $numExercises = count($rows);
        $partOne = file_get_contents($this->mydir . "/html/mmsStartPagejs.html");
        $partOne .=  "<script>var siteURL = '$this->siteURL'</script>" . PHP_EOL;
        $partEndPage = file_get_contents($this->mydir . "/html/mmsEndPagejs.html");
        $partTwo = "";
        $partThree = "";
        $partButton = file_get_contents($this->mydir . "/html/mmsButton.html");
        $partIframe = file_get_contents($this->mydir . "/html/mmsIframejs.html");
        for ( $i = 0; $i < $numExercises; $i++ ) {
            $partTwo .= wp_sprintf($partButton, $i+1, $rows[$i] ['dhnl_aid'], $i+1, $i+1);
            $partThree .= wp_sprintf($partIframe, $i+1);
        }
        $partTwo .= '<h4 id="h1"></h4>';
        $page = $partOne . $partTwo . $partThree . $partEndPage;
        return $page . $this->appendInitScript();
    }

    private function showErrorPage($message)
    {
        $errorPage = file_get_contents($this->mydir . "/html/dhnErrorPage1.html");
        $errorPage = wp_sprintf($errorPage, $message);
        return $errorPage;
    }
    private function getAssignmentId($title)
    {
        global $wpdb;

        $oper = '=';
        if (preg_match("/%$/", $title)) {
            $oper = 'LIKE';
        }
        $table_exercise = $wpdb->prefix . 'dhnl_exercise';
        $rows = $wpdb->get_results("SELECT dhnl_exercise, dhnl_aid FROM " .
            "$table_exercise WHERE dhnl_exercise " .
            "$oper '$title' ORDER BY dhnl_exercise", ARRAY_A);
        return $rows;
    }

    private function appendInitScript()
    {
        $script = '<script>' . PHP_EOL;
        $script .= 'jQuery(window).load(function() {' . PHP_EOL;
        $script .= ' $ = jQuery;' . PHP_EOL;
        $script .= " if (typeof dhnl_init !== 'undefined') {" . PHP_EOL;
        $script .= "  dhnl_init();" . PHP_EOL ;
        $script .= ' }' . PHP_EOL ;
        $script .= '});' . PHP_EOL ;
        $script .= '</script>' . PHP_EOL;
        return $script;
    }
}
new DigitalHearing();
?>
